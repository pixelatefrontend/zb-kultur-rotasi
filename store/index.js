import { newTypes } from '../assets/data/location-types'

const objectify = (obj) => JSON.parse(JSON.stringify(obj))

export const state = () => ({
  locations: [],
  selectedCategory: '',
  locationList: [],
  originalList: [],
  locationFilters: {
    type: null,
    order: null,
    period: null,
  },
  activeLocation: null,
  selectedLocationIndex: -1,
  mapZoomLevel: 14,
  myLocation: null,
})

export const mutations = {
  SET_LOCATIONS(state, data) {
    state.locations = data.map((l) => (l = l.location))

    let locs = [...newTypes]
    locs.map((el) => {
      el.locations = state.locations.filter((d) => d.type === el.id)
    })

    state.locationList = locs
    state.originalList = locs
  },

  ADD_LOCATION(state, data) {
    state.locationsList.push(data)
  },

  SET_MY_LOCATION(state, data) {
    state.myLocation = data
  },

  SET_ACTIVE_CATEGORY(state, data) {
    state.selectedCategory = data
  },

  SET_LOCATION_FILTERS(state, data) {
    state.locationFilters.type = data.type
    state.locationFilters.order = data.order
    state.locationFilters.period = data.period

    if (data.type) {
      if (data.type === 'all') {
        console.log('locatıonLıst: ', state.locationList)
        state.locationList = objectify(state.originalList)
      } else if (data.type === 'feat') {
        state.locationList = objectify(state.originalList)
        state.locationList.forEach((el) => {
          el.locations = el.locations.filter((l) => l.showOnMap === true)
        })
      } else {
        state.locationList = objectify(state.originalList)
        state.locationList = state.locationList.filter((el) => {
          return el.id === data.type
        })
      }
    } else {
      state.locationList = objectify(state.originalList)
    }

    if (data.order) {
      state.locationList.forEach((el) => {
        el.locations.sort((a, b) => {
          if (data.order === 'az') return a.name.tr.localeCompare(b.name.tr)
          if (data.order === 'za') return b.name.tr.localeCompare(a.name.tr)
        })
      })
    }

    if (data.period) {
      if (parseInt(data.period) === 9999) {
        if (!data.order && data.type) {
          state.locationList = objectify(state.originalList)
        }
      } else {
        let items = []
        state.locationList.forEach((el) => {
          items = el.locations.filter((l) => {
            return l.period === parseInt(data.period)
          })

          el.locations = items
        })
      }
    }
  },

  SET_ACTIVE_LOCATION(state, data) {
    state.activeLocation = data
  },

  SET_SELECTED_LOCATION_INDEX(state, data) {
    state.selectedLocationIndex = data
  },

  SET_MAP_ZOOM_LEVEL(state, data) {
    state.mapZoomLevel = data
  },
}

export const actions = {
  fetchLocations: async ({ commit }) => {
    const locListRes = await this.$http.get(
      `${process.env.API_URL}/api/locations`
    )
    const data = await locListRes.json()
    commit('SET_LOCATIONS', data)
  },
}

export const getters = {
  activeCategory(state) {
    return state.selectedCategory
  },

  filteredLocations(state) {
    let locs = []
    if (!state.myLocation) {
      if (state.selectedCategory === '' || state.selectedCategory === 'all') {
        locs = state.locations.filter((el) => el.showOnMap === true)
      } else if (state.selectedCategory === 'feat') {
        locs = state.locations.filter((el) => el.showOnMap === true)
      } else {
        locs = state.locations.filter((el) => {
          let cat = newTypes.find(
            (t) => t.category.indexOf(state.selectedCategory) > -1
          )
          return el.type.includes(cat.id)
        })
      }
    } else {
      let tempLocs = state.locations

      if (state.selectedCategory === '' || state.selectedCategory === 'all') {
        locs = tempLocs
      } else {
        console.log('locs: ', locs)
        locs = tempLocs.filter((el) => {
          let cat = newTypes.find(
            (t) => t.category.indexOf(state.selectedCategory) > -1
          )
          return el.type.includes(cat.id)
        })
      }
    }
    return locs
  },

  selectedLocations(state) {
    return [
      state.locations[22],
      state.locations[12],
      state.locations[8],
      state.locations[9],
    ]
  },

  activeLocation(state) {
    return state.activeLocation
  },

  mapZoomLevel(state) {
    return state.mapZoomLevel
  },
}
