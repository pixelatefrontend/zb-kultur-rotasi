export const dummyLocations =  [
  {
    name: 'Surlar Bölgesi',
    type: 'historical',
    places: [
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
    ],
  },
  {
    name: 'Dini Yapılar',
    type: 'religious',
    places: [
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
    ],
  },
  {
    name: 'Kültür Merkezleri',
    type: 'cultural',
    places: [
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
    ],
  },
  {
    name: 'Sağlık Kurumları',
    type: 'health',
    places: [
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
    ],
  },
  {
    name: 'Parklar ve Bahçeler',
    type: 'parks',
    places: [
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
    ],
  },
  {
    name: 'Eğitim Merkezleri',
    type: 'education',
    places: [
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
    ],
  },
  {
    name: 'Kütüphaneler',
    type: 'library',
    places: [
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
    ],
  },
  {
    name: 'AVM ve Oteller',
    type: 'shop',
    places: [
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
    ],
  },
  {
    name: 'Spor Tesisleri',
    type: 'sport',
    places: [
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
      {
        title: 'Belgrad Gate (Xylokerkos)',
        text: 'Buraya kısa bir açıklama',
        slug: 'belgrad-gate',
      },
    ],
  },
],