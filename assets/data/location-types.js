export const types = [
  {
    id: 'medical',
    name: {
      tr: 'Sağlık Kurumu',
      en: 'Health',
    },
  },
  {
    id: 'rampart',
    name: {
      tr: 'Surlar Bölgesi',
      en: 'City Walls Zone',
    },
  },
  {
    id: 'religious',
    name: {
      tr: 'Dini Yapılar',
      en: 'Religious',
    },
  },
  {
    id: 'culture',
    name: {
      tr: 'Kültür Merkezi',
      en: 'Culture',
    },
  },
  {
    id: 'parks',
    name: {
      tr: 'Park veya Bahçe',
      en: 'Parks',
    },
  },
  {
    id: 'education',
    name: {
      tr: 'Eğitim Merkezi',
      en: 'Education',
    },
  },
  {
    id: 'library',
    name: {
      tr: 'Kütüphane',
      en: 'Library',
    },
  },
  {
    id: 'shop',
    name: {
      tr: 'AVM veya Otel',
      en: 'Shop',
    },
  },
  {
    id: 'sport',
    name: {
      tr: 'Spor Tesisi',
      en: 'Sport',
    },
  },
]

export const newTypes = [
  {
    id: 'all',
    category: ['all'],
    viewbox: '0 0 500 500',
    name: {
      tr: 'Tüm Kategoriler',
      en: 'All Categories ',
    },
  },
  {
    id: 'feat',
    category: ['feat'],
    viewbox: '0 0 500 500',
    name: {
      tr: 'Öne Çıkan Lokasyonlar',
      en: 'Featured Places',
    },
  },
  {
    id: 'historical',
    category: ['historical'],
    viewbox: '0 0 500 500',
    name: {
      tr: 'Tarihi Yapılar',
      en: 'City Walls Zone',
    },
  },
  {
    id: 'religious',
    category: ['religious'],
    viewbox: '0 0 19.381 16.51',
    name: {
      tr: 'Dini Yapılar',
      en: 'Religious Buildings',
    },
  },
  {
    id: 'culturual',
    category: ['culturual', 'museum'],
    viewbox: '0 0 500 500',
    name: {
      tr: 'Kültürel Merkezler & Müzeler',
      en: 'Cultural Centers & Museums',
    },
  },
  // {
  //   id: 'museum',
  //   category: 'culturual,museum',
  //   viewbox: '0 0 15.02 17.767',
  //   name: {
  //     tr: 'Müzeler',
  //     en: 'Museums',
  //   },
  // },
  {
    id: 'tombs',
    category: ['monument', 'tombs'],
    viewbox: '0 0 500 500',
    name: {
      tr: 'Anıt Mezarlar & Türbeler',
      en: 'Cemeteries & Tombs',
    },
  },
  // {
  //   id: 'tombs',
  //   category: 'monument,tombs',
  //   viewbox: '0 0 16.716 19.433',
  //   name: {
  //     tr: 'Türbeler',
  //     en: 'Tombs',
  //   },
  // },
  {
    id: 'food',
    category: ['food'],
    viewbox: '0 0 500 500',
    name: {
      tr: 'Lezzet Durakları',
      en: 'Flavor Corners',
    },
  },
  {
    id: 'shop',
    category: ['shop'],
    viewbox: '0 0 500 500',
    name: {
      tr: 'Alışveriş Noktaları',
      en: 'Shopping Points',
    },
  },
]

export const orders = [
  {
    id: 'az',
    name: {
      tr: 'Alfabetik olarak A-Z',
      en: 'Alphabetically A-Z',
    },
  },
  {
    id: 'za',
    name: {
      tr: 'Alfabetik olarak Z-A',
      en: 'Alphabetically Z-A',
    },
  },
]

export const periods = [
  {
    id: 9999,
    name: {
      tr: 'Tüm Zamanlar',
      en: 'All Times',
    },
  },
  {
    id: 2000,
    name: {
      tr: '2000 Sonrası',
      en: 'After 2000s',
    },
  },
  {
    id: 1500,
    name: {
      tr: '1500-2000 Arası',
      en: '1500s-2000s',
    },
  },
  {
    id: 1000,
    name: {
      tr: '1000-1500 Arası',
      en: '1000s-1500s',
    },
  },
  {
    id: 500,
    name: {
      tr: '500-1000 Arası',
      en: '500s-1000s',
    },
  },
  {
    id: 0,
    name: {
      tr: '0-500 Arası',
      en: '0-500s',
    },
  },
  {
    id: -1,
    name: {
      tr: 'Milattan Önce',
      en: 'Before Christ',
    },
  },
]
