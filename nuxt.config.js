export default {
  dev: process.env.NODE_ENV !== 'production',
  server: {
    port: 8000, // default: 3000
    host: '0.0.0.0', // default: localhost
  },
  static: {
    prefix: false,
  },
  target: 'server',
  head: {
    titleTemplate(titleChunk) {
      console.log('sss: ', process.env.SITE_NAME)
      return titleChunk
        ? titleChunk
        : `${process.env.SITE_NAME} - ${process.env.MUNICIPALITY}`
    },
    meta: [
      {
        charset: 'utf-8',
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1',
      },
      {
        name: 'title',
        content: `${process.env.SITE_NAME} - ${process.env.MUNICIPALITY}`,
      },
      {
        hid: 'description',
        name: 'description',
        content: `${process.env.DESCRIPTION}`,
      },
      {
        hid: 'og:url',
        property: 'og:url',
        content: `https://${process.env.DOMAIN}`,
      },
      {
        hid: 'og:type',
        property: 'og:type',
        content: 'article',
      },
      {
        hid: 'og:title',
        property: 'og:title',
        content: `${process.env.SITE_NAME} - ${process.env.MUNICIPALITY}`,
      },
      {
        hid: 'og:description',
        property: 'og:description',
        content: `${process.env.DESCRIPTION}`,
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: `https://${process.env.DOMAIN}/images/cover.jpg`,
      },
      {
        hid: 'twitter:card',
        property: 'twitter:card',
        content: `https://${process.env.DOMAIN}/images/cover.jpg`,
      },
      {
        hid: 'twitter:title',
        name: 'twitter:title',
        content: `${process.env.SITE_NAME} - ${process.env.MUNICIPALITY}`,
      },
      {
        hid: 'twitter:description',
        name: 'twitter:description',
        content: `${process.env.DESCRIPTION}`,
      },
      {
        hid: 'twitter:creator',
        name: 'twitter:creator',
        content: `${process.env.MUNICIPALITY}`,
      },
      {
        hid: 'twitter:image',
        name: 'twitter:image',
        content: `https://${process.env.DOMAIN}/images/cover.jpg`,
      },
      {
        hid: 'twitter:domain',
        name: 'twitter:domain',
        content: `https://${process.env.DOMAIN}`,
      },
      {
        hid: 'twitter:site',
        name: 'twitter:site',
        content: '@zeytinburnubld',
      },
      {
        name: 'msapplication-TileColor',
        hid: 'msapplication-TileColor',
        content: '#ffffff',
      },
      {
        name: 'msapplication-TileImage',
        hid: 'msapplication-TileImage',
        content: '/ms-icon-144x144.png',
      },
      {
        name: 'theme-color',
        hid: 'theme-color',
        content: '#ffffff',
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'apple-touch-icon',
        sizes: '57x57',
        href: '/apple-icon-57x57.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '60x60',
        href: '/apple-icon-60x60.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '72x72',
        href: '/apple-icon-72x72.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '76x76',
        href: '/apple-icon-76x76.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '114x114',
        href: '/apple-icon-114x114.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '120x120',
        href: '/apple-icon-120x120.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '144x144',
        href: '/apple-icon-144x144.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '152x152',
        href: '/apple-icon-152x152.png',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: '/apple-icon-180x180.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '192x192',
        href: '/android-icon-192x192.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '96x96',
        href: '/android-icon-96x96.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: '/android-icon-32x32.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/android-icon-16x16.png',
      },
      {
        rel: 'manifest',
        href: '/manifest.json',
      },
    ],
  },
  components: true,
  css: ['~/assets/css/global.scss', 'swiper/css/swiper.min.css'],
  plugins: [
    {
      src: '~/plugins/v-click-outside.js',
    },
    {
      src: '~/plugins/qr.js',
      mode: 'client',
      ssr: false,
    },
    {
      src: '~/plugins/lightbox.js',
      mode: 'client',
      ssr: false,
    },
    {
      src: '~/plugins/google-map.js',
      mode: 'client',
    },
    {
      src: '~/plugins/nuxt-swiper.js',
      mode: 'client',
    },
    {
      src: '~/plugins/easy-lightbox.js',
      mode: 'client',
      ssr: false,
    },
    { src: '~/plugins/google-analytics.js', mode: 'client' },
  ],
  modules: ['@nuxt/http', 'nuxt-i18n', '@nuxtjs/sitemap'],
  build: {
    transpile: [/^gmap-vue($|\/)/],
  },
  buildModules: ['@nuxtjs/eslint-module'],
  eslint: {
    cache: false,
  },
  i18n: {
    defaultLocale: process.env.LANGUAGE,
    detectBrowserLanguage: false,
    langDir: 'lang/',
    lazy: true,
    seo: true,
    parsePages: false,
    vueI18nLoader: true,
    pages: {
      lokasyonlar: {
        tr: '/lokasyonlar',
        en: '/places',
      },
      'zeytinburnu-tarihi': {
        tr: '/zeytinburnu-tarihi',
        en: '/zeytinburnu-history',
      },
      'proje-hakkinda': {
        tr: '/proje-hakkinda',
        en: '/about-project',
      },
      arama: {
        tr: '/arama',
        en: '/search',
      },
      'lokasyon/_slug': {
        tr: '/lokasyon/:slug',
        en: '/place/:slug',
      },
    },
    locales: [
      {
        code: 'tr',
        name: 'Türkçe',
        iso: 'tr-TR',
        file: 'tr-TR.js',
        domain: process.env.DOMAIN_TR,
      },
      {
        code: 'en',
        name: 'English',
        iso: 'en-US',
        file: 'en-US.js',
        domain: process.env.DOMAIN_EN,
      },
    ],
    differentDomains: process.env.NODE_ENV === 'production',
    vueI18n: {
      fallbackLocale: process.env.LANGUAGE,
    },
  },
  env: {
    OTHER_LOCALE: process.env.LANGUAGE === 'tr' ? 'en' : 'tr',
    ANALYTIC: process.env.LANGUAGE === 'tr' ? '189779186-3' : '189779186-4',
    API_URL:
      process.env.NODE_ENV !== 'production'
        ? 'http://localhost:3030'
        : 'https://api.kulturvadisi.com',
    SITE_NAME:
      process.env.LANGUAGE === 'tr'
        ? 'Zeytinburnu Kültür Vadisi'
        : 'Zeytinburnu Beyond The Walls',
    MUNICIPALITY:
      process.env.LANGUAGE === 'tr'
        ? 'Zeytinburnu Belediyesi'
        : 'Zeytinburnu Municipality',
    DESCRIPTION:
      process.env.LANGUAGE === 'tr'
        ? "İstanbul, Zeytinburnu ilçesi kültür varlıkları interaktif haritası 'Kültür Vadisi' ile tarihi yapıları ve daha fazlasını keşfet!"
        : "Discover inspiring cultural heritage and more with 'Culture Valley', the interactive map of Istanbul's Zeytinburnu district!",
    DOMAIN:
      process.env.LANGUAGE === 'tr' ? 'kulturvadisi.com' : 'beyondthewalls.ist',
  },
}
