export default () => {
  const script = document.createElement('script')
  script.src = `https://www.googletagmanager.com/gtag/js?id=UA-${process.env.ANALYTIC}`
  script.async = true
  document.getElementsByTagName('head')[0].appendChild(script)
  window.dataLayer = window.dataLayer || []
  function gtag() {
    // eslint-disable-next-line no-undef
    dataLayer.push(arguments)
  }
  gtag('js', new Date())
  gtag('config', `UA-${process.env.ANALYTIC}`)
}
