import Vue from 'vue'
import * as VueGoogleMaps from '~/node_modules/gmap-vue'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAyNZiyKLodLTqpMtDXOo51qC8Kle1qUvw',
    libraries: '',
    language: 'tr',
    region: 'TR',
  },
  installComponents: true,
})
