export default {
  siteUrl: 'https://kulturvadisi.com',
  mainTitle: 'Zeytinburnu Kültür Vadisi',
  introVideoUrl: 'https://www.youtube.com/embed/JbM_X-3-k3w?autoplay=1',
  title: {
    index: 'Zeytinburnu Belediyesi Kültür Vadisi',
  },
  locations: 'Lokasyonlar',
  locationsSubtext:
    'Zeytinburnu ilçesi sınırlarında yer alan kültür varlıklarını keşfet!',
  allCategories: 'Kategori Filtrele',
  zeytinburnuHistory: 'Zeytinburnu Tarihi',
  zeytinburnuMun: 'Zeytinburnu Belediyesi',
  culturePath: 'Proje Hakkında',
  getDirections: 'Yol Tarifi Al',
  locationsPage: {
    title: 'Lokasyonlar',
    description: 'Lokasyon sayfası açıklama',
    subtext:
      'Zeytinburnu ilçesi sınırlarında yer alan tarihi lokasyonları keşfet!',
  },
  copyright: {
    year: '2021 © Zeytinburnu Belediyesi',
    text: 'Her hakkı saklıdır.',
  },
  homeTwitterText:
    "Zeytinburnu'nda yer alan Kültür Varlıklarını şimdi keşfet! Zeytinburnu Belediyesi Kültür Vadisi İnteraktif Haritası",
  homeTwitterUrl: 'kulturvadisi',
  slugTwitterText:
    "harika bir yer! Zeytinburnu'nda görülecek daha çok yer var, şimdi keşfet!",
  closeBy: 'Yakınlarında',
  routePage: {
    title: 'Proje Hakkında',
    description:
      '2005 yılında hayata geçirilen Zeytinburnu Kültür Vadisi Projesi ile Zeytinburnu’nun çehresi tamamen değişiyor.',
    headerText:
      '2005 yılında hayata geçirilen Zeytinburnu Kültür Vadisi Projesi ile Zeytinburnu’nun çehresi tamamen değişiyor.',
    watchPromo: 'Tanıtımı İzle',
    article: {
      title: 'Kıymetli ziyaretçiler,',
      content:
        '<p>Zeytinburnu, geçmişle bugünü, sanayi ile tabiatı, denizle büyük karayollarını, mezarlıklarla capcanlı bir hayatı, İstanbul’un en eski sakinleriyle yeni göçmenleri, her kökenden ve inançtan insanı buluşturan, farklılıkları uyumla birleştiren müstesna bir ilçedir. Zeytinburnu sakinleri olarak bizler, ilçemizin İstanbul, Türkiye ve dünya için ne ifade ettiğini, nasıl bir zenginliğin sahibi olduğumuzun farkında ve bundan dolayı övünç içindeyiz.</p><br />' +
        '<p>Bir zenginlik olarak addettiğimiz kültürel çeşitlilik, Zeytinburnu’nun bütününe yayılmış ve eşsiz bir kültür rotasına dönüşmüştür. Söz gelimi, ilçemizde Yenikapı Mevlevihanesi ya da Seyyidnizam Tekkesi’ni ziyaret ederek Anadolu mayasını oluşturan tasavvuf geleneğinin izlerini görebilirsiniz; aynı zamanda Hz. Meryem’e ithaf edilen ve orjinal adı Zoodokhos Peges yani <em>"hayat bağışlayan kaynak"</em> olan Balıklı Rum Kilisesi’ne yolunuzu düşürerek Rum Ortodoks geleneğini gözlemleme imkanına da sahip olabilirsiniz.</p><br />' +
        '<p>Zeytinburnu sınırları içerisinde bulunan Panorama 1453 Tarih Müzesi’nde İstanbul’un fethinin özel efektlerle canlandırıldığı bir tarihi anda kendinizi bulabilir ya da Zeytinburnu sahiline doğru bir yürüyüş yapmayı tercih edip kısa bir süre önce restorasyon çalışmaları sırasında keşfedilip müzeye dönüştürülen Mozaik Müzesi ve Kazlıçeşme Kültür Sanat’ta hem Roma dönemine ait mozaikleri inceleyip hem de Türk sanat tarihinin önemli isimlerinin koleksiyonlarından oluşturulan sergileri dolaşabilirsiniz.</p><br />' +
        '<p>İstanbul’un kara surlarından, tarihi kapılarına, Erikli Baba Tekkesi’nden Aya Pareskevi Kilisesi’ne Fişekhane’den Türk Dünyası Kültür Mahallesi’ne pek çok tarihi, dini yapı, türbe, anıt mezar; kültürel merkez ve müzeden oluşan bu zengin hat, eminiz ki ilçemizin dünü, bugünü ve geleceğiyle ilgili sizlere ilham verecek; tarihin koynunda ona yaraşır bir gelecek inşa etme gayretimizin canlı resmi olacaktır.</p><br />' +
        '<p><strong>Ömer Arısoy</strong><br> Zeytinburnu Belediye Başkanı</p>',
    },
  },
  historyPage: {
    title: 'Zeytinburnu Tarihi',
    description:
      'Zeytinburnu, tarihî dokusu ve yerel değerleriyle İstanbul’un kültür ve inanç turizmi destinasyonlarının çok kıymetli bir parçasıdır.',
    beingProvince: 'İlçe Olması',
    area: 'Yüzölçümü',
    population: 'Nüfus',
    article: {
      title:
        'Zeytinburnu bugün olduğu gibi, geçmişte de İstanbul’un Batı’ya, Avrupa’ya açılan kapısıydı. Bu konumu nedeniyle farklı kültür, dil ve inançların birleşme noktasıydı. Bünyesinde hep kozmopolit bir demografiyi barındırdı.',
      text:
        'Denize paralel düz topografyası sayesinde çok eskiden beri uluslararası ulaşım ağının içinde kalmayı başardı. Dolayısıyla her zaman bir ticaret ve cazibe merkezi oldu ve belli dönemlerde yoğun göç aldı.',
    },
    timeline: {
      event1: {
        title:
          "Romalılardan Osmanlı dönemine uzanan en önemli ulaşım yolu Zeytinburnu'dan geçiyordu.",
        text:
          "Romalılar tarafından MÖ 2. yüzyılda yapılan ve Adriyatik’i İstanbul’a bağlayan Egnatia yolu Zeytinburnu'dan geçiyordu. Yaklaşık 1200 km uzunluğundaki Egnatia yolu Roma'nın dev ulaşım ağının önemli bir koluydu. Ayrıca imparatorluğun Balkanlar ve Trakya üzerindeki hâkimiyetinin de güçlü bir sembolüydü. Bu güzergah Osmanlı döneminde de önemini korudu.",
      },
      event2: {
        title:
          '5. yüzyılda bölgeye çok önemli dinî ve siyasi yapılar inşa edildi.',
        text:
          '5. yüzyıla gelindiğinde Zeytinburnu Bizans imparatorluğunun gözde yerleşim yerlerinden biri oldu. Bu yüzyılın ikinci yarısında imparator I. Leo Panayia Kilisesi’ni ve hemen yanındaki Balıklı Ayazması’nı inşa ettirdi. Daha sonra imparator I. Iustinianos, bu kiliseyi onararak genişletti ve o günden bugüne bazı patrik cenazeleri buraya defnedildi. Böylece bölge Hıristiyanlık açısından önem kazandı. 9. yüzyıla gelindiğinde, imparator I. Basileios tarafından Balıklı Ayazması civarında inşâ edilen Piyi Sarayı, bölgeyi bir cazibe merkezine dönüştürdü. Zaman içinde Ayazma ve Çırpıcı Çayırı halkın kutsal günlerde ve panayırlarda geldikleri yerler oldu.',
      },
      event3: {
        title:
          'Fatih, Haliç’le Marmara denizi arasında uzanan kara surlarını Yedikule Hisarı’yla takviye ettirdi.',
        text:
          'Fatih Sultan Mehmed İstanbul’u fethettikten sonra Haliç’le Marmara denizi arasındaki kara surlarına takviye olarak Yedikule Hisarı’nı inşâ ettirdi. Böylece Zeytinburnu’nun önemi daha da arttı. Rivayete göre, İstanbul’un fethi sırasında ordu içinde su sıkıntısı yaşandığı bir dönemde gökyüzünde uçuşan kazlar görülür. Kazların görüldüğü yerde yapılan araştırmaların sonunda temiz bir su kaynağı keşfedilir. Bu hadisenin anısına buraya üzerinde kaz kabartması bulunan bir çeşme yaptırılır. O günden itibaren bölge Kazlıçeşme olarak anılır.',
      },
      event4: {
        title:
          '15. yüzyıldan itibaren Kazlıçeşme bölgesi tabakhane, mezbaha ve mumhanelerin merkezi oldu.',
        text:
          'Kazlıçeşme bölgesi, Fatih Sultan Mehmed tarafından kasap ve debbağ esnafına tahsis edildi. Zaman içinde burada irili ufaklı birçok mezbaha ve yüzlerce tabakhane kuruldu. Kesilen hayvanların iç yağlarından mum yapılan mumhaneler de Kazlıçeşme’de yerini aldı. Böylece bölge entegre işleyen bir üretim merkezine dönüştü. İktisadi canlılığa paralel olarak Kazlıçeşme ve çevresinde yerleşim sayısı çoğaldı.',
      },
      event5: {
        title:
          'Zeytinburnu bostanları, mescitleri, tekke ve dergahları, mevlevihanesi ve külliyeleriyle tipik bir Osmanlı şehrine dönüştü.',
        text:
          "Zeytinburnu, İstanbul'un fethinden sonra mescitleri, tekke ve dergahları, mevlevihanesi ve külliyeleri sayesinde tipik bir Osmanlı yerleşimine dönüştü. Kara surları boyunca uzanan meşhur bostanlarında çeşitli sebzeler yetiştiriliyor, İstanbul’un zerzevat ihtiyacı büyük oranda buradan karşılanıyordu. Sur dışı aynı zamanda büyük bir mezarlık alanıydı. İlk olarak Fetih şehitlerinin defnedildiği bu eski Türk mezarlıkları, zaman içinde sanatsal değeri yüksek mezar taşlarıyla âdeta bir açık hava müzesi görünümüne kavuştu.",
      },
      event6: {
        title:
          'Zeytinburnu, Kazlıçeşme Organize Sanayi Bölgesi olarak planlandı ve ağır sanayi bölgelerinden ikisi Zeytinburnu’nda kuruldu.',
        text:
          "Zeytinburnu 19. yüzyılda Osmanlı sanayileşme sürecinin her adımına tanıklık etti. Geleneksel üretim yapan tabakhanelerin yanına yeni teknolojiyle demir, çelik, mavzer-fişek, şimendifer, kimyahane ve dokuma fabrikaları kuruldu. Zeytinburnu Cumhuriyet döneminde de hızlı bir değişime uğradı. Burası 1947 yılında İstanbul Belediyesi tarafından Kazlıçeşme Organize Sanayi Bölgesi olarak planlandı ve ağır sanayi bölgelerinden ikisi Zeytinburnu'nda kuruldu. Sanayileşmeyle birlikte Zeytinburnu, köyden kente göç olgusuyla, akabinde gecekondu mahalleleriyle tanıştı.",
      },
      event7: {
        title: 'Zeytinburnu, 1957 yılında ilçe statüsüne yükseltildi.',
        text:
          'Artan nüfusun etkisiyle birlikte 1957 yılında ilçe statüsüne yükseltildi. 1990’lı yıllara gelindiğinde gecekonduların yerini apartmanlar alırken ilçenin sosyal yapısı da değişti. Bu yılların sonuna doğru ZEYPORT liman projesi hayata geçirildi. 2000’lere yeni bir kimlik arayışında giren ilçe, bu yıllardaki kentsel dönüşüm projeleri, altyapı çalışmaları, çevre düzenlemeleri ve ulaşım yatırımlarıyla yepyeni bir çehreye büründü. Özellikle ilçe sakinlerinin kültürel ve sosyal bakımdan gelişmesini önceleyen belediyecilik hizmetlerine ağırlık verildi. ',
      },
      event8: {
        title:
          'Zeytinburnu, tarihî dokusu ve yerel değerleriyle bugün İstanbul’un önde gelen kültür ve inanç turizmi merkezlerinden biridir.',
        text:
          'Tüm bu çabalar meyvelerini vermeye başladı, günümüzde Zeytinburnu parlatılan tarihî dokusu, yerel değerleriyle İstanbul’un önde gelen kültür ve inanç turizmi destinasyonları içindedir.',
      },
    },
  },
  select: 'Kategoriler',
  categoryFilterTitle: 'Filtrele',
  orderFilterTitle: 'Sırala',
  periodFilterTitle: 'Yapıldığı Dönem',
  showOnMap: 'Haritada Göster',
  showCloseBy: 'Yakınımdakileri Göster',
  foundResults: 'Bulunan Sonuçlar',
  recommendedLocations: 'Önerilen Lokasyonlar',
  farAway: 'Çok Uzaktasın!',
  farAwayContent:
    "Zeytinburnu'na uzak olduğunuz için size yakın lokasyonları gösteremiyoruz.",
  copyText: {
    before: 'Linki Kopyala',
    after: 'Kopyalandı!',
  },
  shortDesc: {
    locations: 'Zeytinburnu Kültür Varlıklarını Keşfet',
    history: 'Dünden Bugüne Zeytinburnu',
    culture: 'Başkanın Mesajı',
  },
  intro: {
    map: {
      title: 'İnteraktif Harita',
      text: 'Zeytinburnu Rehberine Hoş Geldiniz!',
    },
    qr: {
      title: 'QR Kodu Tara!',
      text: 'Kodu Tarayarak Lokasyonu Keşfet!',
    },
  },
  shareOnFacebook: "Facebook'da Paylaş",
  getlink: 'Bağlantıyı Kopyala',
  shareOnTwitter: "Twitter'da Paylaş",
  qrCode: 'QR Kod',
  scanQr: 'QR Kodu Tarat',
  scanningQR: 'Okunuyor...',
  invalidQR:
    'Geçersiz kod! Yalnızca Kültür Vadisi kapsamındaki eserler hakkında bilgi alabilirsiniz.',
  qrResultText: 'QR kodu taratarak lokasyonlar hakkında bilgi edin!',
  allRights: 'Her hakkı saklıdır.',
  noResults: 'Sonuç Bulunamadı!',
  searchPlaceholder: 'Arama yap...',
  searchPlaceholderLong: 'Tarihi, dini, kültürel yapı veya lokasyon ara...',
  filter: 'Kategoriler',
  urls: {
    locations: '/lokasyonlar',
    history: '/zeytinburnu-tarihi',
    valley: '/proje-hakkinda',
    search: '/arama',
    location: '/lokasyon/',
  },
  errorPage: {
    title: 'Sayfa Bulunamadı!',
    content:
      'Erişmeye çalıştığınız sayfa mevcut değil veya kullanımdan kaldırılmış.',
    btnText: 'Anasayfaya Dön',
  },
}
