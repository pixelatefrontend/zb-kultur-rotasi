export default {
  siteUrl: 'https://beyondthewalls.ist',
  mainTitle: 'Zeytinburnu Beyond The Walls',
  introVideoUrl: 'https://www.youtube.com/embed/ZIkugLcssqk?autoplay=1',
  title: {
    index: 'Zeytinburnu Municipality Beyond The Walls',
  },
  locations: 'Places',
  locationsSubtext: 'Discover inspiring cultural heritage in Zeytinburnu!',
  allCategories: 'All Categories',
  zeytinburnuHistory: 'History of Zeytinburnu',
  zeytinburnuMun: 'Zeytinburnu Municipality',
  culturePath: 'About Project',
  getDirections: 'Get Directions',
  locationsPage: {
    title: 'Places',
    description: 'Discover inspiring cultural heritage in Zeytinburnu!',
    subtext: 'Discover inspiring cultural heritage in Zeytinburnu!',
  },
  copyright: {
    year: '2021 © Zeytinburnu Municipality',
    text: 'All rights reserved.',
  },
  homeTwitterText: "Interactive map of Istanbul's Zeytinburnu district!",
  homeTwitterUrl: 'beyondthewalls',
  slugTwitterText:
    'is impressive! There are many more places to see in Zeytinburnu!',
  closeBy: 'Places Nearby',
  routePage: {
    title: 'About Project',
    description:
      '2005 yılında hayata geçirilen Zeytinburnu Kültür Vadisi Projesi ile Zeytinburnu’nun çehresi tamamen değişiyor.',
    headerText:
      'Dericilik sanayini, dokuma sanayi izledi. 1927 yılında Bezmen’ler Kazlıçeşme’de dokuma sanayini kurunca çalışan işçiler yerleşmeye başladılar.',
    watchPromo: 'Watch Promo',
    article: {
      title: 'Dear visitors,',
      content:
        '<p>Zeytinburnu is an exclusive district that bridges the past and the present, industry and nature, sea and the highways, cemeteries, and life, old Istanbulites and new migrants. The district connects people from different beliefs, different ethnic groups and harmonizes the differences appropriately. As the residents of Zeytinburnu, we are aware of what our district stands for Istanbul, Turkey, and the world. We acknowledge and are proud of the value of what we have here.</p><br />' +
        '<p>We internalize cultural diversity as a value and this diversity spreads all across Zeytinburnu and became a unique cultural route. For instance, in our district, you can visit Yenikapı Mevlevi Lodge or Seyyid Nizam Lodge and see the signs of mysticism that formed the core structure of Anatolia. Furthermore, you may have the chance to see the Greek Orthodox tradition by heading to Balıklı the Greek Church of St. Mary of the Spring and originally known as "Zoodochos Peges" meaning "Life-giving Spring", which was dedicated to Mary.</p><br />' +
        '<p>You can find yourself in the Panorama 1453 History Museum (located inside the boundaries of Zeytinburnu) where the conquest of Istanbul is animated with unique effects or you might want to take a walk towards the seaside of Zeytinburnu and visit the Mosaics Room that was recently explored during the restoration processes and transformed into a museum, and Kazlıçeşme Cultural Center where you can both find the mosaics from the Roman era and the compilations and collections of famous Turkish artists.</p><br />' +
        '<p>From the land walls of Istanbul to historical entrance gates, Erikli Baba Dervish Lodge to The Church of Saint Paraskevi, Fişekhane to the Turkish World Cultural Quarter, this valuable line consists of a lot of historic, religious places, tombs, mausoleums, cultural centers, and museums that will undoubtedly inspire you about the past, present and future of our district, and will be the living proof of our efforts to create a suitable future that also remembers the past for Zeytinburnu.</p><br />' +
        '<p><strong>Ömer Arısoy</strong><br> Mayor of Zeytinburnu Municipality</p>',
    },
  },
  historyPage: {
    title: 'Zeytinburnu History',
    description:
      'With its historical characteristics and local values, Zeytinburnu is a precious part of cultural and religious tourism in İstanbul.',
    beingProvince: 'Becoming Province',
    area: 'Area',
    population: 'Population',
    article: {
      title:
        "Zeytinburnu has always been Istanbul's gateway to the West and Europe. Thanks to its location, it was the crossroads of various cultures, languages, beliefs, and religions.  It always welcomed cosmopolitan demographies.",
      text:
        'Thanks to its plain topography parallel to the sea, it has been always inside the international transportation network. Therefore, it was always a center of attraction and an intersection of business activities, and during certain periods, a lot of people immigrated to Zeytinburnu.',
    },
    timeline: {
      event1: {
        title:
          'The most important transportation road that goes all the way from the Romans to the Ottomans would go through Zeytinburnu.',
        text:
          "Via Egnatia, constructed by the Romans in the 2nd century BC and connecting the Adriatic to Istanbul, was passing through Zeytinburnu. Approximately 1,200 km long, Via Egnatia was a critical part of Rome's huge transportation network. It was a solid symbol that represented the Empire's absolute sovereignty on the Balkans and Thrace. This route was still important during the Ottoman era.",
      },
      event2: {
        title:
          'In the 5th century important religious and political structures were built in the region.',
        text:
          'When we came to the 5th century, Zeytinburnu was one of the most appealing residential areas of the Byzantine Empire. In the second half of that century, Leo I the Thracian had the Panagia Church and Balikli Cemetery built side by side. And after that, Justinian I had this church repaired and expanded. Since then, some patriarchs have been buried there. In this way, the region gained importance for Christianity. When we came to the 9th century, Basil I had the Piyi Palace built around the Balıklı Cemetery, and the palace helped the region become a center of attraction. As time advanced, the Cemetery and Çırpıcı Park became places where people visit on the holy days and fairs.',
      },
      event3: {
        title:
          'Mehmed the Conqueror reinforced the land walls between the Golden Horn and the Marmara Sea with the Yedikule Fortress.',
        text:
          'After Mehmed the Conqueror conquered Istanbul, he had the Yedikule Fortress constructed as a fortification for the land walls between the Golden Horn and the Marmara Sea. Thus, Zeytinburnu gained even more importance. Rumour has it that during the conquest of Istanbul when soldiers in the army were suffering from water shortages, people saw gooses flying in the skies. When they searched the area where the gooses were seen, they explored a clean water source. They built a fountain with a goose relief here in memory of this incident. Since then, the region is called and known as Fountain with Goose (Kazlıçeşme).',
      },
      event4: {
        title:
          'Since the 15th century, Kazlıçeşme became a center for taweries, slaughterhouses, and candle facilities.',
        text:
          'Mehmed the Conqueror allocated the Fountain with Goose, especially for butchers and tanners. As the time went by, hundreds of large and small slaughterhouses and taweries were built there. Kazlıçeşme also hosted candle facilities, where people produce candles with suets obtained from the slaughtered animals. Thus, the region became a production center that operates in an integrated way. Thanks to the economic boom, the number of residential areas in and around Kazlıçeşme increased.',
      },
      event5: {
        title:
          'Zeytinburnu became a typical Ottoman town with its orchards, mosques, lodges, Islamic monasteries, the Mevlevi lodge, and Islamic-ottoman social complexes.',
        text:
          'After the conquest of Istanbul, Zeytinburnu became a typical Ottoman residential area with its mosques, lodges, Islamic monasteries, Mevlevi lodges, and Islamic-ottoman social complexes. People would grow a variety of vegetables in the famous orchards located parallel to the land walls, and use these vegetables to fulfill the need of Istanbulites. There was a large cemetery outside of the walls, too. These ancient Turkish cemeteries, where the martyrs of the Conquest were first buried gained the look of an open-air museum with gravestones of high artistic value in time.',
      },
      event6: {
        title:
          'Zeytinburnu was planned as Kazlıçeşme Organized Industrial Site and two heavy industry sites were established in Zeytinburnu.',
        text:
          'Zeytinburnu witnessed every single stage of the Ottoman industrialization process in the 19th century. Next to the taweries with conventional production; iron, steel, revolver/cartridge, railroad factories, textile mills, and chemistry laboratories operating with the new technology were established. Zeytinburnu underwent a fast change during the Republic period as well. This area was planned as the Kazlıçeşme Organized Industrial Site by the Istanbul Municipality in 1947 and thus, two heavy industry sites were established in Zeytinburnu. With industrialization and migration from the countryside to town, some slums appeared in Zeytinburnu.',
      },
      event7: {
        title: 'Zeytinburnu became a district in 1957.',
        text:
          'As the population grew, Zeytinburnu was promoted and became a district in 1957. When we came to the 1990s, slums were replaced by apartment blocks, leading to a change in the social structure of Zeytinburnu. Towards the end of the 1990s, a port project called ZEYPORT was completed. Entering the 2000s with a new identity seek, the district gained a brand-new form with urban transformation projects, infrastructure efforts, environmental planning activities, and transportation investments during these years. Municipality services prioritized the cultural and social development of the residents.',
      },
      event8: {
        title:
          'With its historic characteristics and local values, Zeytinburnu is one of the outstanding cultural and religious destinations of Istanbul today.',
        text:
          'All these efforts created positive outcomes and Zeytinburnu became the foremost cultural and religious destination of Istanbul with its polished historic characteristics and local values.',
      },
    },
  },
  select: 'Categories',
  categoryFilterTitle: 'Filter',
  orderFilterTitle: 'Sort By',
  periodFilterTitle: 'Construction Period',
  showOnMap: 'Show On Map',
  showCloseBy: 'Show Nearby Places',
  foundResults: 'Found',
  recommendedLocations: 'Recommended Places',
  farAway: "You're far away!",
  farAwayContent:
    'Unfortunately, we cannot share places near you because you are so far away.',
  copyText: {
    before: 'Get Link',
    after: 'Copied!',
  },
  shortDesc: {
    locations: 'Discover Cultural Heritage',
    history: 'From the Past to the Present',
    culture: 'Message From The President',
  },
  intro: {
    map: {
      title: 'Interactive Map',
      text: 'Welcome to Zeytinburnu Guide!',
    },
    qr: {
      title: 'Scan QR',
      text: 'Explore By Scanning QR!',
    },
  },
  shareOnFacebook: 'Share on Facebook',
  getlink: 'Get Link',
  shareOnTwitter: 'Share on Twitter',
  qrCode: 'QR Code',
  scanQr: 'Scan QR',
  scanningQR: 'Scanning...',
  invalidQR: 'Invalid QR!',
  qrResultText: 'Scan QR to get information about the places!',
  allRights: 'All rights reserved.',
  noResults: 'There were no results!',
  searchPlaceholder: 'Search...',
  searchPlaceholderLong:
    'Search for historical, cultural or religious places from past to present...',
  filter: 'Filter',
  urls: {
    locations: '/places',
    history: '/zeytinburnu-history',
    valley: '/about-project',
    search: '/search',
    location: '/place/',
  },
}
