export default function ({ store }) {
  if (!store.state.locations.length < 0) {
    store.dispatch('fetchLocations')
  }
}
